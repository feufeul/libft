#include "libft.h"

t_dict				*ft_new_dict(char *key, char *data)
{
	t_dict			*head;

	if (!(head = (t_dict*)malloc(sizeof(t_dict))))
		return (NULL);
	head->next = NULL;
	if (!(head->data = ft_strdup(data)))
	{
		free(head);
		return (NULL);
	}
	if (!(head->key = ft_strdup(key)))
	{
		free(head->data);
		free(head);
		return (NULL);
	}
	return (head);
}

int					ft_push_dict(t_dict **head, char *key, char *data)
{
	t_dict			*tmp;

	tmp = *head;
	while (tmp && tmp->next)
	{
		if (!ft_strcmp(tmp->key, key))
			return (0);
		tmp = tmp->next;
	}
	if (!(tmp->next = ft_new_dict(key, data)))
		return (0);
	return (1);
}

int					ft_delete_dict(t_dict **head, char *key)
{
	t_dict			*tmp;
	t_dict			*next;

	if (!head || !*head || !key)
		return (0);
	tmp = *head;
	next = tmp->next;
	if (!(ft_strcmp(tmp->key, key)))
	{
		ft_strdel(&tmp->data);
		ft_strdel(&tmp->key);
		free(tmp);
		*head = next;
		return (1);
	}
	while (tmp && tmp->next)
	{
		if (!ft_strcmp(next->key, key))
		{
			tmp->next = next->next;
			ft_strdel(&next->data);
			ft_strdel(&next->key);
			free(next);
			return (1);
		}
		tmp = tmp->next;
		next = next->next;
	}
	return (0);
}
