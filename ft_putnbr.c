/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/11 17:58:37 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/13 14:55:05 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_printing_nb(int digit, int nb)
{
	int		tmp;

	while (digit > 0)
	{
		digit--;
		tmp = nb / ft_exponant(10, digit);
		ft_putchar(tmp + '0');
		nb = nb - tmp * ft_exponant(10, digit);
	}
}

void		ft_putnbr(int nb)
{
	if (nb == -2147483648)
		ft_putstr("-2147483648");
	else if (nb == 0)
		ft_putchar('0');
	else
	{
		if (nb < 0)
		{
			nb = -nb;
			ft_putchar('-');
		}
		ft_printing_nb(ft_get_digit(nb), nb);
	}
}
