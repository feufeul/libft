/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strcpy.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/04 16:10:05 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/06 12:10:02 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcpy(char *dst, const char *src)
{
	char	*ptr_dst;

	ptr_dst = dst;
	while (*src)
	{
		*ptr_dst = *src;
		src++;
		ptr_dst++;
	}
	*ptr_dst = '\0';
	return (dst);
}
