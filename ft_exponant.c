/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exponant.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/11 18:07:30 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/16 14:40:00 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

unsigned int			ft_exponant(int base, unsigned int exp)
{
	unsigned long long	result;

	result = 1;
	while (exp > 0)
	{
		result *= base;
		exp--;
	}
	if (result > 4294967295)
		return (0);
	return ((unsigned int)result);
}
