/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/05 13:15:33 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/11 18:05:51 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	unsigned char	*str;
	unsigned char	*src_str;
	size_t			cpt;

	cpt = 0;
	str = (unsigned char*)dst;
	src_str = (unsigned char*)src;
	while (cpt < n)
	{
		str[cpt] = src_str[cpt];
		if (src_str[cpt] == (unsigned char)c)
			return (dst + cpt + 1);
		cpt++;
	}
	if (cpt == n)
		return (0);
	return (dst);
}
