/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/11 13:01:24 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/18 11:24:07 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_get_words(char const *s, char c)
{
	int		cpt;
	int		counting;

	cpt = 0;
	counting = 0;
	while (s && *s)
	{
		if (*s != c && !counting)
		{
			cpt++;
			counting++;
		}
		if (*s == c && counting)
			counting = 0;
		s++;
	}
	return (cpt);
}

static int		ft_word_delim(char const *s, char c)
{
	int		size;

	size = 0;
	while (*s && *s != c)
	{
		size++;
		s++;
	}
	return (size);
}

static void		ft_add_word(char *dest, char const *src, int size)
{
	int i;

	i = 0;
	while (i < size)
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
}

static void		ft_free_tab_char(char **tab, int i)
{
	int		cpt;

	cpt = 0;
	while (cpt < i)
		free(tab[cpt++]);
}

char			**ft_strsplit(char const *s, char c)
{
	int		words;
	int		i_s;
	int		i_r;
	char	**result;

	words = ft_get_words(s, c);
	i_s = -1;
	if (!s || !(result = (char**)malloc(sizeof(char*) * words + 1)))
		return (NULL);
	i_r = 0;
	while (words != 0 && s[++i_s] && i_r < words)
	{
		if (s[i_s] != c)
		{
			if (!(result[i_r] = (char*)malloc(ft_word_delim(s + i_s, c) + 1)))
			{
				ft_free_tab_char(result, i_r);
				return (NULL);
			}
			ft_add_word(result[i_r++], s + i_s, ft_word_delim(s + i_s, c));
			i_s += ft_word_delim(s + i_s, c);
		}
	}
	result[i_r] = 0;
	return (result);
}
