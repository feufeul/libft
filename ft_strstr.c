/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/05 14:59:12 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/11 17:30:57 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *haystack, const char *needle)
{
	char	*ptr_hay;
	size_t	size;

	ptr_hay = (char*)haystack;
	size = ft_strlen((char*)needle);
	if (*needle == 0)
		return (ptr_hay);
	while (*ptr_hay)
	{
		if (!ft_memcmp(ptr_hay, needle, size))
			return (ptr_hay);
		ptr_hay++;
	}
	return (0);
}
