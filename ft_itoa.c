/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/11 11:02:25 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/11 11:35:22 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoa(int n)
{
	char	*final_str;
	int		i;

	i = ft_get_digit(n);
	if (!(final_str = ft_strnew(i)))
		return (NULL);
	if (n == -2147483648)
		return (ft_strdup("-2147483648"));
	if (n < 0)
	{
		*final_str = '-';
		n = -n;
	}
	if (!n)
		*final_str = '0';
	while (n)
	{
		final_str[--i] = n % 10 + '0';
		n /= 10;
	}
	return (final_str);
}
