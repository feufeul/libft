/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelone.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 12:49:19 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/18 11:27:31 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdelone(t_list **alst, void (*del)(void*, size_t))
{
	t_list	*tmp;

	if (!alst || !del)
		return ;
	tmp = *alst;
	del(tmp->content, tmp->content_size);
	tmp->content = NULL;
	free(*alst);
	*alst = NULL;
}
