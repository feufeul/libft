/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 11:22:10 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/12 14:49:09 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*link;

	link = NULL;
	if (!(link = (t_list*)malloc(sizeof(t_list))))
		return (NULL);
	link->next = NULL;
	if (!content)
	{
		link->content = NULL;
		link->content_size = 0;
	}
	else
	{
		if (!(link->content = (void*)malloc(content_size)))
		{
			free(link);
			return (NULL);
		}
		link->content = ft_memcpy(link->content, content, content_size);
		link->content_size = content_size;
	}
	return (link);
}
