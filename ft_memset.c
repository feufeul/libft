/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/04 13:06:02 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/05 11:13:00 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	unsigned char	*result;
	size_t			i;

	result = (unsigned char*)b;
	i = 0;
	while (i < len)
	{
		result[i] = c;
		i++;
	}
	return (b);
}
