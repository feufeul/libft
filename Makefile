# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/10/03 16:13:53 by cvan-duf          #+#    #+#              #
#    Updated: 2018/10/18 10:38:48 by cvan-duf         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc

CFLAGS = -Wall -Wextra -Werror

NAME = libft.a

SRCS = ft_atoi ft_bzero ft_exponant ft_get_digit ft_isalnum \
	   ft_isalpha ft_isascii ft_isdigit ft_isprint ft_itoa \
	   ft_lstadd ft_lstdel ft_lstdelone ft_lstiter ft_lstmap \
	   ft_lstnew ft_memalloc ft_memccpy ft_memchr ft_memcmp \
	   ft_memcpy ft_memdel ft_memmove ft_memset ft_putchar \
	   ft_putchar_fd ft_putendl ft_putendl_fd ft_putnbr \
	   ft_putnbr_fd ft_putstr ft_putstr_fd ft_strcat ft_strchr \
	   ft_strclr ft_strcmp ft_strcpy ft_strdel ft_strdup \
	   ft_strequ ft_striter ft_striteri ft_strjoin ft_strlcat \
	   ft_strlen ft_strmap ft_strmapi ft_strncat ft_strncmp \
	   ft_strncpy ft_strnequ ft_strnew ft_strnstr ft_strrchr \
	   ft_strsplit ft_strstr ft_strsub ft_strtrim ft_tolower \
	   ft_toupper ft_dict

SRCS_FILES = $(addsuffix .c, $(SRCS))

OBJ = $(SRCS_FILES:.c=.o)

all: $(NAME)

%.o: %.c libft.h
	$(CC) $(CFLAGS) -c -o $@ $<

$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)

re: fclean all

.PHONY: re clean fclean all
