/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/04 18:19:53 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/16 11:53:18 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *s1, const char *s2, size_t n)
{
	size_t	cpt;
	size_t	size_s1;
	size_t	size_s2;

	cpt = 0;
	size_s1 = ft_strlen(s1);
	size_s2 = ft_strlen(s2);
	if (n < size_s1 + 1)
		return (size_s2 + n);
	while (cpt < n - size_s1 - 1 && s2[cpt])
	{
		s1[size_s1 + cpt] = s2[cpt];
		cpt++;
	}
	s1[size_s1 + cpt] = '\0';
	return (size_s1 + size_s2);
}
