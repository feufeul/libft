/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 13:28:44 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/18 11:27:17 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void*, size_t))
{
	t_list	*tmp;
	t_list	*prev;

	if (!alst || !del)
		return ;
	tmp = *alst;
	while (tmp)
	{
		del(tmp->content, tmp->content_size);
		tmp->content = NULL;
		prev = tmp;
		tmp = tmp->next;
		free(prev);
		prev = NULL;
	}
	*alst = NULL;
}
